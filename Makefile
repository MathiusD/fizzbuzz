default_target: img_exec

#Ici on demande le nettoyage complet de notre dossier
cleanRepo: clean module_clean img_clean

#Ici on demande l'execution de notre jar
exec: jar
	@java -jar FizzBuzz.jar

#Ici on demande la compilation de nos fichiers FizzBuzzer et FizzBuzzApp
build: clean buildInternal buildExec

#Ici on demande la compilation de notre fichier FizzBuzzer
buildInternal:
	@javac fizzbuzz/internal/FizzBuzzer.java -d build/

#Ici on demande la compilation de notre fichier FizzBuzzApp
buildExec:
	@javac fizzbuzz/app/FizzBuzzApp.java -d build -cp build/

#Ici on utilise nos fichiers compilés afin de créer notre jar
jar: build
	@cd build && jar cfe ../FizzBuzz.jar fizzbuzz/app/FizzBuzzApp .

#Ici on nettoye le dépôt avant de compiler quoi que ce soit
clean:
	@rm -rf build/
	@rm -f FizzBuzz.jar

#Ici on nettoye le dépôt avant de compiler notre module
module_clean:
	@rm -rf out/
	@rm -f FizzBuzzModulaire.jar

#Ici on compile notre module
module_build: module_clean
	@javac -d out --module-source-path . --module-path /opt/jdk-12.O.2/jmods --module fr.iutna.od

#Ici on crée une jar modulaire à l'aide de notre module compilé
module_jar: module_build
	@jar --create --file=FizzBuzzModulaire.jar --main-class=fr.iutna.od.fizzbuzz.app.FizzBuzzApp -C out/fr.iutna.od/ .

#Ici on execute notre jar modulaire
module_exec: module_jar
	@java --module-path opt/jdk-12.0.2/jmods -jar FizzBuzzModulaire.jar

#Ici on nettoye le dépôt avant de créer notre image
img_clean:
	@rm -rf img/

#Ici on prépare notre image
img_build: module_build img_clean
	@jlink --output img/ --strip-debug --module-path out --add-modules fr.iutna.od

#Ici on execute FizzBuzzApp à travers notre image
img_exec: img_build
	@img/bin/java --module fr.iutna.od/fr.iutna.od.fizzbuzz.app.FizzBuzzApp