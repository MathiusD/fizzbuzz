module fr.iutna.od{

    requires java.base;
    requires java.logging;

    exports fr.iutna.od.fizzbuzz.app;
    exports fr.iutna.od.fizzbuzz.internal;
}