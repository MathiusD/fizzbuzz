package fr.iutna.od.fizzbuzz.internal;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;
import java.util.logging.Logger;

public class FizzBuzzer
{
    private List<Function<Integer, String>> fizzBuzzFuncs;
    private static Logger log = Logger.getLogger(FizzBuzzer.class.getName());

    public FizzBuzzer()
    {
        fizzBuzzFuncs = null;
        //log.addHandler(new ConsoleHandler());
    }

    public void addFizzBuzz(Function<Integer, String> fn)
    {
        /*
        Méthode pour ajouter un filtre à l'objet FizzBuzzer.
        */
        log.info("Adding filter to FizzBuzz : " + fn.toString());
        if (fizzBuzzFuncs == null)
        {
            fizzBuzzFuncs = new ArrayList<Function<Integer,String>>();
        }
        fizzBuzzFuncs.add(fn);
    }

    public String feed(int v)
    {
        /*
        Méthode qui retroune la chaine de caractères retournée par les filtres si aucun filtre
        ne renvoi quoique ce soit la méthode renvoie alors une chaine de caractère correspondant
        à l'entier passé en argument.
        */
        String fizzBuzzWords = "";
        for(Function<Integer, String> fn : fizzBuzzFuncs)
        {
            fizzBuzzWords = fizzBuzzWords.concat(fn.apply(v));
        }
        if (fizzBuzzWords.equals(""))
        {
            fizzBuzzWords = String.valueOf(v);
        }

        return fizzBuzzWords;
    }

}
