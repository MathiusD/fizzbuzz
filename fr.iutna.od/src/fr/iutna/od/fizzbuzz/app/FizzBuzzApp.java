package fr.iutna.od.fizzbuzz.app;
import fr.iutna.od.fizzbuzz.internal.FizzBuzzer;

public class FizzBuzzApp
{
    public static void main(String[] args)
    {
        //On déclare et on instancie un objet FizzBuzzer
        FizzBuzzer fbz = new FizzBuzzer();

        //On ajoute un filtre pour tout les nombres divisible par 3 afin qu'ils affichent "Fizz" sinon rien.
        fbz.addFizzBuzz(x -> (x % 3) == 0 ? "Fizz" : "");
        //On ajoute un filtre pour tout les nombres divisible par 7 afin qu'ils affichent "Buzz" sinon rien.
        fbz.addFizzBuzz(x -> (x % 7) == 0 ? "Buzz" : "");
        //On ajoute un filtre pour tout les nombres dont le carré est divisible par 2 afin qu'ils affichent "- Kamoulox" sinon rien.
        fbz.addFizzBuzz(x -> ((x*x) % 2) == 0 ? "- Kamoulox -" : "");

        for (int i=1; i<100; i++)
        {
            //On affiche le résultat du traitement du nombre par notre objet FizzBuzzer pour tout les nombres de 1 à 99
            System.out.println(fbz.feed(i));
        }
        System.exit(0);
    }
}
